package com.developer.oauthauthorizationserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OauthAuthorizationServer {

	public static void main(String[] args) {
		SpringApplication.run(OauthAuthorizationServer.class, args);
	}

}
