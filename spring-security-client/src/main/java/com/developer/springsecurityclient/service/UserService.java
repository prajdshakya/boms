package com.developer.springsecurityclient.service;

import com.developer.springsecurityclient.dto.ResponseDTO;
import com.developer.springsecurityclient.dto.UsersDTO;

public interface UserService {
    ResponseDTO saveUser(UsersDTO usersDTO);

    ResponseDTO getUsers();

    ResponseDTO getUserById(int id);
}
