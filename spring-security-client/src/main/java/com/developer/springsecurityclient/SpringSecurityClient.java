package com.developer.springsecurityclient;

import com.developer.springsecurityclient.entity.Role;
import com.developer.springsecurityclient.entity.Users;
import com.developer.springsecurityclient.repository.RoleRepo;
import com.developer.springsecurityclient.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCrypt;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class SpringSecurityClient {

	@Autowired
	RoleRepo roleRepo;
	@Autowired
	UserRepo userRepo;

	public static void main(String[] args) {
		SpringApplication.run(SpringSecurityClient.class, args);
	}

	//Creating admin user with role "ADMIN" if doesnt exists
	@PostConstruct
	public void CreateAdminUserWithAdminRoleIfNotExists(){
		createRole();
		addAdminUser();
	}

	public void createRole(){
		List<Role> roleList = new ArrayList<>();
		List<Role> roles = roleRepo.findAll();
		if (roles.isEmpty()){
			Role adminRole = new Role();
			adminRole.setType("ADMIN");
			roleList.add(adminRole);

			Role inserterRole = new Role();
			inserterRole.setType("INSERTER");
			roleList.add(inserterRole);

			Role handlerRole = new Role();
			handlerRole.setType("HANDLER");
			roleList.add(handlerRole);
		}
		roleRepo.saveAll(roleList);
	}
	public void addAdminUser(){
		Users user = userRepo.findByUsername("admin");
		if (user==null){
			Users adminUser =  new Users();
			adminUser.setUserName("admin");
			int workload = 12;
			String salt = BCrypt.gensalt(workload);
			String hashed_password = BCrypt.hashpw("admin@1234", salt);
			adminUser.setPassword(hashed_password);
			Users savedUser = userRepo.save(adminUser);
			Role role = roleRepo.findByRoleType();
			if (role!=null){
				userRepo.saveRegisterUserRole(savedUser.getId(), role.getId());
			}
		}
	}


}
