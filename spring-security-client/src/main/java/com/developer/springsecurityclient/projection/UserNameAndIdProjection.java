package com.developer.springsecurityclient.projection;

public interface UserNameAndIdProjection {
    int getId();
    String getUserName();

}
