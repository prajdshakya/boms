package com.developer.springsecurityclient.repository;

import com.developer.springsecurityclient.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleRepo extends JpaRepository<Role, Integer> {
    @Query(nativeQuery = true, value = "select * from role r where r.type = 'ADMIN'")
    Role findByRoleType();

    @Query(nativeQuery = true, value = "select r.* from user_roles ur \n" +
            "join users u on u.id = ur.user_id \n" +
            "join role r on r.id = ur.role_id where u.id = ?1")
    List<Role> findRoleByUserId(int id);
}
