package com.developer.springsecurityclient.repository;

import com.developer.springsecurityclient.entity.Users;
import com.developer.springsecurityclient.projection.UserNameAndIdProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface UserRepo extends JpaRepository<Users, Integer> {
    @Query(nativeQuery = true, value = "insert into user_roles (user_id, role_id) values (?1,?2)")
    @Modifying
    @Transactional
    void saveRegisterUserRole(int id, int roleId);

    @Query(nativeQuery = true, value = "select * from users u where u.user_name = ?1")
    Users findByUsername(String username);

    @Query(nativeQuery = true, value = "select u.id, u.user_name as \"userName\" from users u")
    List<UserNameAndIdProjection> findAllUsers();
}
